package com.example.demo.controller;

import com.example.demo.dto.ApiErrorDto;
import com.example.demo.dto.CourseDto;
import com.example.demo.dto.Marker;
import com.example.demo.exception.CourseNotFoundException;
import com.example.demo.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @PostMapping("")
    @Validated({Marker.OnCourseCreate.class})
    public CourseDto createCourse(@Valid @RequestBody CourseDto courseDto) throws CourseNotFoundException {
        return courseService.createCourse(courseDto);
    }

    @PutMapping("")
    @Validated({Marker.OnCourseUpdate.class})
    public CourseDto updateCourse(@Valid @RequestBody CourseDto courseDto) throws CourseNotFoundException {
        return courseService.updateCourse(courseDto);
    }

    @DeleteMapping("/{id}")
    public CourseDto deleteCourse(@PathVariable("id") Long id) {
        return courseService.deleteCourse(CourseDto.builder().id(id).build());
    }

    @GetMapping("")
    public List<CourseDto> getAllCourses() {
        return courseService.getAllCourses();
    }

    @GetMapping("/filter")
    public List<CourseDto> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix") String titlePrefix) {
        return courseService.findByTitleWithPrefix(titlePrefix);
    }

    @GetMapping("/{id}")
    public CourseDto getCourse(@PathVariable("id") Long id) throws CourseNotFoundException {
        return courseService.getCourse(CourseDto.builder().id(id).build());
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorDto> courseNotFoundExceptionHandler(CourseNotFoundException ex) {
        return new ResponseEntity<>(
                new ApiErrorDto(OffsetDateTime.now(), ex.getMessage()),
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorDto> argumentsValidationExceptionHandler(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(
                new ApiErrorDto(OffsetDateTime.now(), ex.getMessage()),
                HttpStatus.BAD_REQUEST
        );
    }
}