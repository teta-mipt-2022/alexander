package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.OffsetDateTime;

@AllArgsConstructor
@Builder(toBuilder = true)
@Data
public class ApiErrorDto {
    public OffsetDateTime dateOccurred;
    public String message;
}
