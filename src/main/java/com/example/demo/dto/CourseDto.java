package com.example.demo.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder(toBuilder = true)
@Data
public class CourseDto {
    @NotBlank(message = "Course id has to be filled", groups = {Marker.OnCourseUpdate.class})
    public long id;
    @NotBlank(message = "Course author has to be filled", groups = {Marker.OnCourseCreate.class, Marker.OnCourseUpdate.class})
    public String author;
    @NotBlank(message = "Course title has to be filled", groups = {Marker.OnCourseCreate.class, Marker.OnCourseUpdate.class})
    public String title;
}
