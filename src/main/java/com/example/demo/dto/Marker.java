package com.example.demo.dto;

public interface Marker {
    interface OnCourseCreate {}
    interface OnCourseUpdate {}
}
