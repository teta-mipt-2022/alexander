package com.example.demo.repository;

import com.example.demo.dto.CourseDto;
import com.example.demo.exception.CourseNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CourseRepository {
    private final List<CourseDto> courses = Collections.synchronizedList(new ArrayList<>());
    private final AtomicLong idGenerator = new AtomicLong(1);

    public CourseDto findById(CourseDto courseDto) throws CourseNotFoundException {
        Optional<CourseDto> result = courses.stream()
                .filter(c -> c.getId() == courseDto.getId())
                .findFirst();
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new CourseNotFoundException("Course doesn't exist (id=" + courseDto.getId() + ")");
        }
    };

    public List<CourseDto> findAll() {
        return new ArrayList<>(courses);
    }

    public CourseDto save(CourseDto courseDto) throws CourseNotFoundException {
        if (courseDto.getId() == 0) {
            courseDto.setId(idGenerator.incrementAndGet());
            courses.add(courseDto.toBuilder().build());
            return courseDto;
        }
        else {
            for (CourseDto c : courses) {
                if (c.getId() == courseDto.getId()) {
                    c.setAuthor(courseDto.getAuthor());
                    c.setTitle(courseDto.getTitle());
                    return courseDto;
                }
            }
            throw new CourseNotFoundException("Course doesn't exist (id=" + courseDto.getId() + ")");
        }
    }

    public CourseDto delete(CourseDto courseDto) {
        try {
            CourseDto containedCourseDto = findById(courseDto);
            courses.remove(containedCourseDto);
            return containedCourseDto;
        } catch (CourseNotFoundException ignored) {
            return courseDto;
        }
    }

    public List<CourseDto> findByTitleWithPrefix(String prefix) {
        return courses.stream()
                .filter(course -> course.getTitle().startsWith(prefix))
                .collect(Collectors.toList());
    }
}
