package com.example.demo.service;

import com.example.demo.dto.CourseDto;
import com.example.demo.exception.CourseNotFoundException;
import com.example.demo.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {
    @Autowired
    CourseRepository repository;
    public List<CourseDto> getAllCourses() {
        return repository.findAll();
    }
    public CourseDto createCourse(CourseDto courseDto) throws CourseNotFoundException {
        return repository.save(courseDto);
    }
    public CourseDto getCourse(CourseDto courseDto) throws CourseNotFoundException {
        return repository.findById(courseDto);
    }
    public CourseDto updateCourse(CourseDto courseDto) throws CourseNotFoundException {
        return repository.save(courseDto);
    }
    public CourseDto deleteCourse(CourseDto courseDto) {
        return repository.delete(courseDto);
    }
    public List<CourseDto> findByTitleWithPrefix(String prefix) {
        return repository.findByTitleWithPrefix(prefix);
    }
}
